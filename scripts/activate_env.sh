#!/usr/bin/env bash

echo "--- Start ---"

export OTB_RESULT_DIR=/home/thibaut/Documents/output/bv-net
export OTB_DATA_DIR=/home/thibaut/OTB/otb/Modules/Remote/OTBBioVars/data
export PYTHONPATH=/home/thibaut/OTB/build/lib/otb/python
export OTB_APPLICATION_PATH=/home/thibaut/OTB/build/lib/otb/applications