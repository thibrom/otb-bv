#!/usr/bin/env bash

echo "--- Start ---"

export OTB_RESULT_DIR=/home/travis/build/tromain/otb-bv/data/OutputTest
export OTB_DATA_DIR=/home/travis/build/tromain/otb-bv/data
export PYTHONPATH=/opt/otb/lib/otb/python
export OTB_APPLICATION_PATH=/home/travis/build/tromain/otb-bv/install/lib:/opt/otb/lib/otb/applications
